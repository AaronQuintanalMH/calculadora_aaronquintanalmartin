package calculadora_aaronquintanal;

import java.util.Scanner;

public class Calculadora_AaronQuintanal {
    
    static Scanner scanner = new Scanner(System.in);
    static int opcion = -1;
    static double numero1 = 0, numero2 = 0;
    

    public static void main(String[] args) {
        
        while(opcion != 0){
            try{
                System.out.println("Elige opción:\n" +""
                        +"1.-Sumar\n"
                        +"2.-Restar\n"
                        +"3.-Multiplicar\n"
                        +"4.-Dividir\n"
                        +"5.-Porcentaje\n"
                        +"0.-Salir");
                
                System.out.println("Selecciona una opción de 0 a 5");
                opcion = Integer.parseInt(scanner.nextLine());
                
                switch(opcion){
                    case 1:
                        pideNumeros();
                        System.out.println(numero1+" + "+numero2+" = "+(numero1+numero2)); 
                    break;
                    
                    case 2:
                        pideNumeros();
                        System.out.println(numero1+" - "+numero2+" = "+(numero1-numero2));
                    break;
                    
                    case 3:
                        pideNumeros();
                        System.out.println(numero1+" * "+numero2+" = "+(numero1*numero2));
                    break;
                    
                    case 4:
                        pideNumeros();
                        System.out.println(numero1+" / "+numero2+" = "+(numero1/numero2));
                    break;
                    
                    case 5:
                        pideNumeros();
                        System.out.println(numero1+" % "+numero2+" = "+((numero1/numero2)*100));
                    break;
                    
                    case 0:
                        System.out.println("Saliendo...");
                    break;
                    
                    default:
                        System.out.println("Opción no disponible.");
                    break;
                }
            } catch(Exception e) {
                System.out.println("Error!");
            }     
        }
    }
    
        public static void pideNumeros(){
            System.out.println("Introduce el priemr número");
            numero1 = scanner.nextDouble();
            
            System.out.println("Introduce el segundo número");
            numero2 = scanner.nextDouble();
        }
    
}
